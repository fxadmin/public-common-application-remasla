FROM golang:alpine AS builder

WORKDIR /go/src/remasla/

COPY main.go ./

RUN go get -d -v github.com/mediocregopher/radix.v2/redis && \
    CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o remasla .

FROM scratch

COPY --from=builder /go/src/remasla/remasla /

USER 65534:65534

ENTRYPOINT ["/remasla"]
