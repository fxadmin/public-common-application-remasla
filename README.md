# ReMaSla
[![](https://images.microbadger.com/badges/version/fxinnovation/remasla.svg)](https://microbadger.com/images/fxinnovation/remasla "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/fxinnovation/remasla.svg)](https://microbadger.com/images/fxinnovation/remasla "Get your own image badge on microbadger.com")
## Description
Redis Make Slave is a small golang application that has been written to help you handle launching redis clusters on `kubernetes` as `statefulset`.

*This project was inpired/copied from https://github.com/dhilipkumars/redis-sentinel-micro and adapted to fit our needs*

## Tags
We do not publish a `latest` tag. As this will be a running container it's version should be pinned down.

We do publish a `master` tag that should only be used for testing unreleased versions.

## Usage
Usage is restricted with kubernetes. This container should be used as an init container used for a redis cluster.

## Versionning
We're are doing semantic versionning on this repository

## Comments and Issues
If you have comments or detect an issue, please be adviced we don't check the docker hub comments. You can always contact us through the repository.
